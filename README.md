# arch-workstation provisioning

simple script to provision my arch linux workstation.
pull the repo -> run the script -> enjoy.


## req
 - network connection
 - root user access
 - git installed

## what does it do?

  - network configuration (netctl profile)
  - dotfile linking
  - software installation (ansible)
